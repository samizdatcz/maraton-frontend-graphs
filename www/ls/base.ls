alldata = null
loadData = (cb) ->
  rand = Math.random!
  toFloats = (row) ->
    for field, value of row
      row[field] = if value
        parseFloat value
      else
        null
    row
  (err, data) <~ d3.tsv "https://samizdat.cz/maraton-data/data.tsv?rand=#{rand}", toFloats
  alldata := data
  for datum in data
    if datum.mox2
      datum.mox2 = datum.mox2 / 2094 * 2070 * 3.6
  for graph in graphs
    graph.update data

updateAll = ->
  rand = Math.random!
  lastdatum = alldata[*-1]
  (err, datum) <~ d3.json "https://samizdat.cz/maraton-data/lastCoords.json?rand=#{rand}"
  return unless datum
  return if datum.time is lastdatum.time
  for field of lastdatum
    console.log "NO" field if datum[field] is void
  alldata.push datum
  if datum.mox2
    datum.mox2 = datum.mox2 / 2094 * 2070 * 3.6
  for graph in graphs
    graph.update alldata

body = d3.select ig.containers.base


container = body.append \div
if window.location.hash == '#bigfont'
  container.classed \bigfont yes
highlighter = container.append \div
  ..attr \class "highlighter invisible"
  ..append \div
    ..attr \class \start
  ..append \div
    ..attr \class \end
hours = highlighter.append \span
  ..attr \class \hours

toDouble = ->
  s = it.toString!
  if s.length == 1
    s = "0#s"
  s


graphs =
  new ig.Graph container, {field: "hb", unit: "úderů / min" format: -> it}
  new ig.Graph container, {field: "mox", unit: "okysličení svalu" format: -> it.toFixed 1 .replace '.' ','}
  new ig.Graph container, {field: "mox2", unit: "průtok krve" format: -> it.toFixed 2 .replace '.' ','}
  new ig.Graph container, {field: "spd", unit: "km/h" format: -> (it * 3.6).toFixed 1 .replace '.' ','}
  new ig.Graph container, {field: "p" invertAxis: yes, unit: "hPa", format: -> it.toFixed 2 .replace '.' ','}
nowDate = new Date!
for graph in graphs
  graph.on \mousemove (pxX, time) ->
    for graph in graphs
      graph.setMouseMoveHighlight pxX
    # console.log d3.event
    highlighter
      ..classed \invisible no
      ..style \left "#{pxX}px"
    container.classed \highlighter-visible yes
    nowDate.setTime time
    hours.html "#{toDouble nowDate.getHours!}:#{toDouble nowDate.getMinutes!}:#{toDouble nowDate.getSeconds!}"
  graph.on \mouseout ->
    for graph in graphs
      graph.setHighlight null
    highlighter.classed \invisible yes
    container.classed \highlighter-visible no

bigNumbers = container.selectAll \.big-number-container

onScroll = ->
  left = (document.body.scrollLeft || document.documentElement.scrollLeft)
  left += window.innerWidth - 350
  bigNumbers.style \left "#{left}px"
window.addEventListener \scroll onScroll
onScroll!

loadData!
setInterval updateAll, 5000
